//
//  ViewController.swift
//  XTAlertExample
//
//  Created by Di Wu on 4/6/16.
//  Copyright © 2016 Di Wu. All rights reserved.
//

import UIKit
import XTAlert

extension UIColor {
  convenience init(hex: String, alpha: CGFloat) {
    var hexString = hex
    if hexString.hasPrefix("#") {
      hexString = hexString.substring(from: hexString.characters.index(hexString.startIndex, offsetBy: 1))
    }
    
    if hexString.characters.count != 6 {
      self.init(white: 1, alpha: 1)
    } else {
      var hexValue: UInt32 = 0
      Scanner(string: hexString).scanHexInt32(&hexValue)
      self.init(red:   CGFloat((hexValue & 0xFF0000) >> 16) / 0xFF,
                green: CGFloat((hexValue & 0x00FF00) >> 8) / 0xFF,
                blue:  CGFloat(hexValue & 0x0000FF) / 0xFF,
                alpha: alpha)
    }
  }
  
  convenience init(hex: String) {
    self.init(hex: hex, alpha: 1)
  }
}

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  func confirm() {
    print("confirmed!")
  }

  func cancel() {
    print("cancelled!")
  }
  
  @IBAction func show(_ sender: AnyObject) {
    XTAlertView(style: AlertStyle.success,
                title: AlertTitle.text(title: "开启通知"),
                message: "您的剪切板里没有找到链接，复制任何链接，点击‘＋’都可加入收藏夹",
                cancelButtonTitle: "忽略",
                confirmButtonTitle: "打开通知",
                confirmation: { [weak self] in self?.confirm() }).show()
  }
  
  
  @IBAction func showImageTitle(_ sender: AnyObject) {
    XTAlertView(style: AlertStyle.success,
                title: AlertTitle.localImage(image: UIImage(named: "pop-shipping-notification")!),
                message: "您的剪切板里没有找到链接，复制任何链接，点击‘＋’都可加入收藏夹",
                cancelButtonTitle: "我知道了").show()
  }

  @IBAction func showWarningAlert(_ sender: AnyObject) {
    XTAlertView(style: AlertStyle.warning,
                title: AlertTitle.text(title: "开启通知"),
                message: "您的剪切板里没有找到链接，复制任何链接，点击‘＋’都可加入收藏夹",
                cancelButtonTitle: "忽略",
                confirmButtonTitle: "打开通知",
                confirmation: { [weak self] in self?.confirm() }).show(animated: false)
  }
  
  @IBAction func showErrorAlert(_ sender: AnyObject) {
    XTAlertView(style: AlertStyle.error,
                title: AlertTitle.text(title: "On Religion"),
                message: "Your daily life is your temple and your religion. \nWhenever you enter into it take with you your all.\n \t\t\t\t\t- Kahlil Gibran",
                cancelButtonTitle: "Cancel",
                cancellation: { [weak self] in self?.cancel() },
                confirmButtonTitle: "Okay",
                confirmation: { [weak self] in self?.confirm() }).show()
  }
  
  @IBAction func showCustomAlert(_ sender: AnyObject) {
    XTAlertView(style: AlertStyle.custom(colors: [UIColor(hex: "#2980b9"), UIColor(hex: "#3498db")]),
                title: AlertTitle.text(title: "Rational Me"),
                message: "At least once a day, allow yourself the freedom to think and dream for yourself. \n \t\t\t\t- Albert Einstein",
                cancelButtonTitle: "Got it").show()
  }

  @IBAction func showRemoteImageAlert(_ sender: AnyObject) {
      XTAlertView(style: AlertStyle.success,
                  title: AlertTitle.remoteImage(urlString: "http://moment.u.qiniudn.com/pop-pay_again.png"),
                  message: "您的剪切板里没有找到链接，复制任何链接，点击‘＋’都可加入收藏夹",
                  cancelButtonTitle: "我知道了").show()
  }
  

  @IBAction func showImageWithTitle(_ sender: Any) {
//    XTAlertView.titleCoverColor = UIColor.green
    XTAlertView(style: AlertStyle.success, title: AlertTitle.imageTitle(UIImage(named: "pop-shipping-notification")!, "自己査定を一時中止しますか？"), message: "您的剪切板里没有找到链接，复制任何链接，点击‘＋’都可加入收藏夹", cancelButtonTitle: "我知道了").show()
  }

  @IBAction func showImageWithTitleWithNoAutodismiss(_ sender: Any) {
     XTAlertView(style: AlertStyle.success,
                title: AlertTitle.imageTitle(UIImage(named: "pop-shipping-notification")!, "自己査定を一時中止しますか？"),
                message: "您的剪切板里没有找到链接，复制任何链接，点击‘＋’都可加入收藏夹",
                cancelButtonTitle: "我知道了",
                cancellation: { [weak self] in self?.cancel() },
                confirmButtonTitle: "Okay",
                confirmation: { [weak self] in self?.confirm() },
                autoDismiss: false
      ).show()
  }

}

