//
//  XTColor.swift
//  XTAlert
//
//  Created by Di Wu on 4/16/16.
//  Copyright © 2016 Di Wu. All rights reserved.
//

import UIKit

enum XTColor: String {
  case DarkGreen = "00B21D", LightGreen = "76D173"
  case DarkYello = "F57C00", LightYello = "FF972B"
  case DarkRed = "D63A49", LightRed = "F64353"
  case Dark = "333333", Gray = "666666"
  case Light = "999999" , Lighter = "EEEEEE"
  case DarkCover = "011627"
}

extension UIColor {
  convenience init(xta_hex: String, alpha: CGFloat) {
    var hexString = xta_hex
    if hexString.hasPrefix("#") {
      hexString = hexString.substring(from: hexString.characters.index(hexString.startIndex, offsetBy: 1))
    }

    if hexString.characters.count != 6 {
      self.init(white: 1, alpha: 1)
    } else {
      var hexValue: UInt32 = 0
      Scanner(string: hexString).scanHexInt32(&hexValue)
      self.init(red:   CGFloat((hexValue & 0xFF0000) >> 16) / 0xFF,
                green: CGFloat((hexValue & 0x00FF00) >> 8) / 0xFF,
                blue:  CGFloat(hexValue & 0x0000FF) / 0xFF,
                alpha: alpha)
    }
  }

  convenience init(xta_hex: String) {
    self.init(xta_hex: xta_hex, alpha: 1)
  }
}

extension UIColor {
  class func xtColor(_ type: XTColor, alpha: CGFloat = 1) -> UIColor {
    return XTAlert.UIColor(xta_hex: type.rawValue, alpha: alpha)
  }
}
