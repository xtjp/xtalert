//
//  XTAlert.swift
//  XTAlert
//
//  Created by Di Wu on 4/6/16.
//  Copyright © 2016 Di Wu. All rights reserved.
//

import UIKit

enum DismissDirection {
  case top
  case bottom

  func translation(_ size: CGSize) -> CGPoint {
    let screenSize = UIScreen.main.bounds.size
    switch self {
    case .top:
      return CGPoint(x: 0, y: -size.height*1.5)
    case .bottom:
      return CGPoint(x: 0, y: screenSize.height + size.height/2)
    }
  }
}

///
public enum AlertStyle {
  case success
  case warning
  case error
  case custom(colors: [UIColor])
  
  var tintColors: [CGColor] {
    switch self {
    case .success:
      return [UIColor.xtColor(.DarkGreen).cgColor,
              UIColor.xtColor(.LightGreen).cgColor]
    case .warning:
      return [UIColor.xtColor(.DarkYello).cgColor,
              UIColor.xtColor(.LightYello).cgColor]
    case .error:
      return [UIColor.xtColor(.DarkRed).cgColor,
              UIColor.xtColor(.LightRed).cgColor]
    case .custom(let colors):
      return colors.map { $0.cgColor }
    }
  }
  
  var messageParagraphStyle: NSParagraphStyle {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 5
    return paragraphStyle
  }
}

final class DummyRootViewController: UIViewController {
  override var shouldAutorotate: Bool {
    return true
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.isUserInteractionEnabled = false
  }
}

final class XTAlertContainerWindow: UIWindow {
  override init(frame: CGRect) {
    super.init(frame: frame)
    if !UIAccessibilityIsReduceTransparencyEnabled() {
      backgroundColor = UIColor.clear
      // add background blur
      let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
      let blurEffectView = UIVisualEffectView(effect: blurEffect)
      blurEffectView.frame = frame
      blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      self.addSubview(blurEffectView)
    } else {
      backgroundColor = UIColor.white
    }
    rootViewController = DummyRootViewController(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }


}

/// An alternative AlertView, with image title and coloring title bar.

final public class XTAlertView: UIView {
  typealias Action = () -> ()
  typealias ActionItem = (String, Action)
  
  static let maxWidth: CGFloat = 0.75 * min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
  static let kUpdateNewTitleNotification = "kXTANeedsUpdateToNewTitle"

  public static var titleCoverColor = UIColor.xtColor(.DarkCover).withAlphaComponent(0.7)
  
  var containerWindow: XTAlertContainerWindow?
  var dismissAction: Action?
  
  let style: AlertStyle
  var title: AlertTitle {
    didSet {
      titleView = title.view
      xt_loadViews()
      setNeedsLayout()
      layoutIfNeeded()
      realign()
    }
  }
  var titleView: UIView! {
    didSet {
      titleView.setGradientBackground(style.tintColors)
    }
  }
  
  let contentView: UIView = {
    let view = UIView(frame: .zero)
    view.backgroundColor = UIColor.white
    return view
  }()
  
  let messageLabel: UILabel = {
    
    let label = UILabel(frame:
      CGRect(x: 0, y: 0, width: XTAlertView.maxWidth - 30, height: CGFloat.greatestFiniteMagnitude))
    label.textColor = UIColor.xtColor(.Gray)
    label.lineBreakMode = .byWordWrapping
    label.font = UIFont.systemFont(ofSize: 16)
    label.numberOfLines = 0
    return label
  }()
  
  let cancelButton: XTAlertButton
    = XTAlertButton(style: XTAlertButtonStyle.confirm)
  let confirmButton: XTAlertButton
    = XTAlertButton(style: XTAlertButtonStyle.confirm)

  /// Convenience method for initializing an XTAlert view.
  public init(
    style: AlertStyle,
    title: AlertTitle,
    message: String,
    cancelButtonTitle: String,
    cancellation onCancel: (() -> ())? = nil,
    confirmButtonTitle: String? = nil,
    confirmation onConfirm: (() -> ())? = nil,
    autoDismiss: Bool = true
  ) {

    self.style = style
    self.title = title

    super.init(frame: .zero)

    titleView = title.view
    if case .remoteImage(let url) = title {
      addRemoteImageTask(url)
    }


    titleView.setGradientBackground(style.tintColors)

    let styledMessage = NSMutableAttributedString(string: message)
    styledMessage.addAttribute(NSParagraphStyleAttributeName,
                               value: style.messageParagraphStyle,
                               range: NSRange(location: 0, length: styledMessage.length))
    messageLabel.attributedText = styledMessage
    messageLabel.sizeToFit()

    func wrapDismiss(_ force: Bool = true, _ action: Action? = nil) -> Action {
      return { [unowned self] in
        if force { self.dismissAction?() }
        action?()
      }
    }

    cancelButton.actionItem = (cancelButtonTitle, wrapDismiss(autoDismiss, onCancel))
    if let title = confirmButtonTitle {
      confirmButton.actionItem = (title, wrapDismiss(true, onConfirm))
    }

    if autoDismiss {
      // add pan gesture
      let gesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
      addGestureRecognizer(gesture)
    }

    xt_loadViews()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  @objc
  fileprivate func handlePanGesture(_ sender: UIPanGestureRecognizer) {

    let diffY = sender.translation(in: window).y

    if let windowHeight = window?.height {
      self.setY((windowHeight + diffY - height)/2)
    }

    if case .ended = sender.state {
      let velocity = sender.velocity(in: window)
      if velocity.y > 100 {
        dismiss()
      } else if velocity.y < -100 {
        dismiss(animated: true, .top)
      } else {
        realign()
      }
    }
  }

  fileprivate func addRemoteImageTask(_ urlString: String) {
    guard let url = URL(string: urlString) else { return }
    DispatchQueue.global().async {
      URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
        guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
          let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
          let data = data, error == nil,
          let image = UIImage(data: data)
          else { return }
        DispatchQueue.main.async { [weak self] in
          self?.title = AlertTitle.localImage(image: image)
        }
      }).resume()
    }
  }

  func xt_loadViews() {
    backgroundColor = UIColor.clear
    layer.shadowOffset = .zero
    layer.shadowOpacity = 0.15
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowRadius = 10

    setSize(CGSize(width: XTAlertView.maxWidth,
      height: titleView.height + max(messageLabel.height + 30, title.textMinHeight) + cancelButton.height))
    contentView.frame = self.bounds
    contentView.layer.cornerRadius = 6
    contentView.layer.masksToBounds = true
    self.addSubview(contentView)

    contentView.addSubview(titleView)
    contentView.addSubview(messageLabel)
    contentView.addSubview(cancelButton)
    contentView.addSubview(confirmButton)
  }
  
  override public func layoutSubviews() {
    super.layoutSubviews()

    var left: CGFloat = 0, top: CGFloat = 0

    titleView.setOrigin(CGPoint(x: left, y: top))
    top += titleView.height + 15

    messageLabel.setOrigin(CGPoint(x: (XTAlertView.maxWidth - messageLabel.width)/2, y: top))
    top += messageLabel.height + 15
    top += max(messageLabel.height + 30, title.textMinHeight) - (messageLabel.height + 30)

    // The cancel button is always exist, it follows confirm style when confirm button doesn't exist
    // and it will follow cancel style when confirm button shows up.
    if confirmButton.actionItem != nil {
      cancelButton.style = XTAlertButtonStyle.cancel
      cancelButton.frame = CGRect(x: left, y: top, width: XTAlertView.maxWidth/2, height: cancelButton.height)
      cancelButton.borders = [.Top]

      confirmButton.frame = CGRect(x: left + cancelButton.width, y: top,
                               width: XTAlertView.maxWidth/2, height: confirmButton.height)
      confirmButton.borders = [.Top, .Left]
    } else {
      cancelButton.frame = CGRect(x: left, y: top, width: XTAlertView.maxWidth, height: cancelButton.height)
      cancelButton.borders = [.Top]
      confirmButton.frame = .zero
      confirmButton.borders = []
    }
  }
  
}

extension XTAlertView {

  func xtDispatchOnMainThread(_ block: XTAlertView.Action) {
    if Thread.isMainThread {
      block()
    } else {
      DispatchQueue.main.sync(execute: { block() })
    }
  }

  func realign() {
    xtDispatchOnMainThread { [unowned self] in
      UIView.animate(withDuration: 0.35,
        delay: 0,
        usingSpringWithDamping: 0.7,
        initialSpringVelocity: 0.7,
        options: .allowUserInteraction,
        animations: { [unowned self] in
          if let _window = self.containerWindow {
            self.setX((_window.width - self.width) / 2)
            self.setY((_window.height - self.height) / 2)
          }
          self.alpha = 0.99
        },
        completion: { _ in }
      )
    }
  }

  ///
  public func show(animated: Bool = true) {
    dismiss(animated: false)
    containerWindow = XTAlertContainerWindow(frame: UIScreen.main.bounds)
    containerWindow!.windowLevel = UIWindowLevelAlert
    containerWindow!.addSubview(self)
    dismissAction = { [weak self] in
      self?.dismiss(animated: animated)
    }
    xtDispatchOnMainThread { [unowned self] in
      if animated {
        self.center = CGPoint(x: self.containerWindow!.bounds.midX, y: 0)
        self.alpha = 0

        UIView.animate(withDuration: 0.7,
          delay: 0,
          usingSpringWithDamping: 0.7,
          initialSpringVelocity: 0.7,
          options: [.curveEaseIn, .allowUserInteraction],
          animations: { [unowned self] in
            self.transform = CGAffineTransform(translationX: 0, y: self.containerWindow!.bounds.midY)
            self.alpha = 0.99
          },
          completion: { _ in }
        )
      } else {
        self.center = CGPoint(x: self.containerWindow!.bounds.midX,
                              y: self.containerWindow!.bounds.midY)
      }
      self.containerWindow!.makeKeyAndVisible()
    }
  }

  func dismiss(animated: Bool = true, _ direction: DismissDirection = .bottom) {
    xtDispatchOnMainThread { [unowned self] in
      if animated {
        UIView.animate(withDuration: 0.25,
          delay: 0,
          options: [.curveEaseIn],
          animations: { [unowned self] in
            let translation = direction.translation(self.frame.size)
            self.transform = CGAffineTransform(translationX: translation.x, y: translation.y)
            self.alpha = 0
          },
          completion: { [unowned self] (_) in
            self.containerWindow?.isHidden = true
            self.containerWindow = nil
          }
        )
      } else {
        self.containerWindow?.isHidden = true
        self.containerWindow = nil
      }
    }
  }
}
