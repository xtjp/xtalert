//
//  XTAlertButton.swift
//  XTAlert
//
//  Created by Di Wu on 4/8/16.
//  Copyright © 2016 Di Wu. All rights reserved.
//

import UIKit

struct XTAlertButtonBorder: OptionSet {
  let rawValue: Int
  init(rawValue: Int) {
    self.rawValue = rawValue
  }
  static let Top = XTAlertButtonBorder(rawValue: 1)
  static let Right = XTAlertButtonBorder(rawValue: 2)
  static let Bottom = XTAlertButtonBorder(rawValue: 4)
  static let Left = XTAlertButtonBorder(rawValue: 8)
  
  static func border() -> UIView {
    let view = UIView(frame: .zero)
    view.backgroundColor = UIColor.xtColor(.Lighter)
    return view
  }
}

enum XTAlertButtonStyle {
  case cancel
  case confirm
  
  var textFont: UIFont {
    switch self {
    case .cancel:
      return UIFont.systemFont(ofSize: 16)
    case .confirm:
      return UIFont.boldSystemFont(ofSize: 16)
    }
  }
  
  var textColor: UIColor {
    switch self {
    case .cancel:
      return UIColor.xtColor(.Light)
    case .confirm:
      return UIColor.xtColor(.Dark)
    }
  }
  
  var textHighlightedColor: UIColor {
    return UIColor.xtColor(.DarkGreen)
  }
  
}

final class XTAlertButton: UIButton {

  var style: XTAlertButtonStyle
  var actionItem: XTAlertView.ActionItem?
  var borders: XTAlertButtonBorder = []
  
  let topBorder = XTAlertButtonBorder.border()
  let leftBorder = XTAlertButtonBorder.border()
  let bottomBorder = XTAlertButtonBorder.border()
  let rightBorder = XTAlertButtonBorder.border()
  
  init(style: XTAlertButtonStyle, actionItem: XTAlertView.ActionItem? = nil) {
    self.style = style
    super.init(frame: CGRect(x: 0, y: 0, width: XTAlertView.maxWidth, height: 44))
    addTarget(self, action: #selector(xt_done),
                   for: UIControlEvents.touchUpInside)
    addSubview(topBorder)
    addSubview(leftBorder)
    addSubview(rightBorder)
    addSubview(bottomBorder)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel?.font = style.textFont
    setTitleColor(style.textColor, for: UIControlState())
    setTitleColor(style.textHighlightedColor, for: UIControlState.highlighted)
    setTitle(actionItem?.0, for: UIControlState())
    
    if borders.contains(.Top) {
      topBorder.setSize(CGSize(width: width, height: 1))
    }
    
    if borders.contains(.Left) {
      leftBorder.setSize(CGSize(width: 1, height: height))
    }
    
    if borders.contains(.Bottom) {
      bottomBorder.frame = CGRect(x: 0, y: self.height - 1, width: width, height: 1)
    }
    
    if borders.contains(.Right) {
      rightBorder.frame = CGRect(x: width - 1, y: 0, width: 1, height: height)
    }
  }
  
  func xt_done() {
    actionItem?.1()
  }
}

