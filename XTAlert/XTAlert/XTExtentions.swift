//
//  XTExtentions.swift
//  XTAlert
//
//  Created by Di Wu on 4/8/16.
//  Copyright © 2016 Di Wu. All rights reserved.
//

import UIKit

extension UIView {
  var x:          CGFloat { return frame.origin.x }
  var y:          CGFloat { return frame.origin.y }
  var width:      CGFloat { return frame.size.width }
  var height:     CGFloat { return frame.size.height }
  
  func setSize(_ size: CGSize) {
    frame.size = size
  }
  
  func setOrigin(_ point: CGPoint) {
    frame.origin = point
  }
  
  func setX(_ x: CGFloat) {
    frame.origin = CGPoint(x: x, y: frame.origin.y)
  }
  
  func setY(_ y: CGFloat) {
    frame.origin = CGPoint(x: frame.origin.x, y: y)
  }
  
  func setWidth(_ width: CGFloat) {
    frame.size.width = width
  }
  
  func setHeight(_ height: CGFloat) {
    frame.size.height = height
  }
}

extension UIView {
  func setGradientBackground(_ colors: [CGColor]) {
    let name = "XTAlert.gradient"
    let layer: CAGradientLayer = CAGradientLayer()
    layer.name = name
    layer.frame = bounds
    layer.colors = colors
    layer.startPoint = CGPoint(x: 0, y: 0.5)
    layer.endPoint = CGPoint(x: 1, y: 0.5)
    if let oldLayer = self.layer.sublayers?.filter({$0.name == name}).first {
      self.layer.replaceSublayer(oldLayer, with: layer)
    } else {
      self.layer.insertSublayer(layer, at: 0)
    }
  }
}
