//
//  XTAlertTitle.swift
//  XTAlert
//
//  Created by Di Wu on 4/8/16.
//  Copyright © 2016 Di Wu. All rights reserved.
//

import UIKit

///
public enum AlertTitle {
  case imageTitle(UIImage, String)
  case localImage(image: UIImage)
  case remoteImage(urlString: String)
  case text(title: String)
  
  var bounds: CGRect {
    switch self {
    case .localImage(let image):
      let ratio = XTAlertView.maxWidth / max(image.size.width, image.size.height)
      return CGRect(x: 0, y: 0, width: XTAlertView.maxWidth, height: image.size.height * ratio)
    case .imageTitle(let image, _):
      let ratio = XTAlertView.maxWidth / max(image.size.width, image.size.height)
      return CGRect(x: 0, y: 0, width: XTAlertView.maxWidth, height: image.size.height * ratio)
    case .text, .remoteImage:
      return CGRect(x: 0, y: 0, width: XTAlertView.maxWidth, height: 44)
    }
  }
  
  var textMinHeight: CGFloat {
    switch self {
    case .localImage, .imageTitle:
      return bounds.height * 0.6
    case .text, .remoteImage:
      return XTAlertView.maxWidth * 0.33
    }
  }

  var view: UIView {
    let backgroudView = UIView(frame: bounds)
    backgroudView.backgroundColor = UIColor.clear
    switch self {
    case .localImage(let image):
      let imageView = UIImageView(image: image)
      imageView.frame = bounds
      imageView.contentMode = .scaleAspectFill
      imageView.clipsToBounds = true
      backgroudView.addSubview(imageView)
      
      let bottomBorder = CALayer()
      bottomBorder.backgroundColor = UIColor.xtColor(.Lighter).cgColor
      bottomBorder.frame = CGRect(x: 0, y: backgroudView.height - 1, width: backgroudView.width, height: 1)
      backgroudView.layer.addSublayer(bottomBorder)

    case let .imageTitle(image, title):
      let imageView = UIImageView(image: image)
      imageView.frame = bounds
      imageView.contentMode = .scaleAspectFill
      imageView.clipsToBounds = true
      backgroudView.addSubview(imageView)

      let coverView = UIView(frame: imageView.bounds)
      coverView.backgroundColor = XTAlertView.titleCoverColor
      imageView.addSubview(coverView)

      let label = UILabel(frame: CGRect(x: 20, y: 0, width: imageView.bounds.width - 40, height: imageView.bounds.height))
      label.text = title
      label.numberOfLines = 2
      label.textColor = UIColor.white
      label.textAlignment = .center
      label.font = UIFont.boldSystemFont(ofSize: 16)
      imageView.addSubview(label)

    case .text(let title):
      let label = UILabel(frame: bounds)
      label.text = title
      label.font = UIFont.boldSystemFont(ofSize: 16)
      label.textColor = UIColor.white
      label.textAlignment = .center

      backgroudView.addSubview(label)
    default: ()
    }
    backgroudView.clipsToBounds = true
    return backgroudView
  }
}


